package org.nufail.onthisday;

import org.json.JSONObject;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OnThisDayDAO {

    private static Logger logger = Logger.getLogger(OnThisDayDAO.class.getName());

    public void handleSubscriptionNotification(DataSource dataSource, JSONObject jsonRequest) throws SQLException {
        Connection con = null;
        try {
            con = dataSource.getConnection();
            saveSubscriptionNotification(con, jsonRequest);
            updateSubscription(con, jsonRequest);
            logger.info("Subscription Notification handled successfully");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error while handling Subscription Notification", e);
        } finally {
            if (con != null) {
                con.close();
            }
        }

    }

    private void saveSubscriptionNotification(Connection con, JSONObject jsonRequest) throws SQLException {
        PreparedStatement ps = null;
        String sql = "INSERT INTO sub_notifications (subscriberId,status,applicationId,frequency,version,timeStamp) " +
                "VALUES (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, jsonRequest.getString("subscriberId"));
            ps.setString(2, jsonRequest.getString("status"));
            ps.setString(3, jsonRequest.optString("applicationId"));
            ps.setString(4, jsonRequest.optString("frequency"));
            ps.setString(5, jsonRequest.optString("version"));
            ps.setString(6, jsonRequest.optString("timeStamp"));

            ps.executeUpdate();
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    private void updateSubscription (Connection con, JSONObject jsonRequest) throws SQLException {
        PreparedStatement ps = null;
        String sql = "INSERT INTO subscriptions (subscriberId,status,applicationId,frequency,version,timeStamp) " +
                "VALUES (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE status=VALUES(status),applicationId=VALUES(applicationId)," +
                "frequency=VALUES(frequency),version=VALUES(version),timeStamp=VALUES(timeStamp)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, jsonRequest.getString("subscriberId"));
            ps.setString(2, jsonRequest.getString("status"));
            ps.setString(3, jsonRequest.optString("applicationId"));
            ps.setString(4, jsonRequest.optString("frequency"));
            ps.setString(5, jsonRequest.optString("version"));
            ps.setString(6, jsonRequest.optString("timeStamp"));

            ps.executeUpdate();
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

}
