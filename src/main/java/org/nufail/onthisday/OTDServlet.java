package org.nufail.onthisday;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

public class OTDServlet extends HttpServlet {

	private static final long serialVersionUID = -7846657161312911467L;
	private static Logger logger = Logger.getLogger(OTDServlet.class.getName()); 

	private static DataSource datasource = null;

	@Override
	public void init() throws ServletException {
		logger.info("init OTDServlet");
		try {
			InitialContext initCtx = new InitialContext();
			datasource = (DataSource) initCtx.lookup("java:/comp/env/jdbc/otddb");
			if (datasource == null) {
				String msg = "Could not find otddb DataSource";
				logger.severe(msg);
				throw new ServletException(msg);
			}
		} catch (NamingException e) {
			logger.log(Level.SEVERE, "Error while initializing otddb DataSource", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("Subscribe request received");
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = req.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		logger.info("Subscribe request - " + sb.toString());
		try {
			JSONObject jsonObject = new JSONObject(sb.toString());
			OnThisDayDAO dao = new OnThisDayDAO();
			dao.handleSubscriptionNotification(datasource, jsonObject);

			resp.setContentType("application/json");
			PrintWriter writer = resp.getWriter();
			writer.println(getSuccessResponse());
		} catch (JSONException e) {
			logger.log(Level.SEVERE, "Invalid JSON string in subscription notification", e);
		} catch (SQLException e) {
			logger.log(Level.WARNING, "Error while handling subscription notification", e);
		}
	}

	private String getSuccessResponse() {
		JSONObject response = new JSONObject();
		response.put("statusCode", "S1000");
		response.put("statusDetails", "Success");
		return response.toString();
	}

	@Override
	public void destroy() {

	}

}
