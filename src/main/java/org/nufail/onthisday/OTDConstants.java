package org.nufail.onthisday;

import java.util.Properties;

public class OTDConstants {

	public static final String OTD_PROPERTY_FILE = "org/nufail/onthisday/OTDconf.properties";	
	public static final String CRON_PROPERTY = "otd.trigger.cron";
	public static final String APP_ID_PROPERTY = "otd.app.id";
	public static final String APP_PASS_PROPERTY = "otd.app.pass";
	public static final String YEAR_BOUND_PROPERTY = "otd.year.boundary";
	public static final String SMS_ENDPOINT_PROPERTY = "otd.sms.endpoint";
	
	private static Properties propertyMap;

	public static void setPropertyMap(Properties propertyMap) {
		OTDConstants.propertyMap = propertyMap;
	}
	
	public static String getProperty(String key){
		return propertyMap.getProperty(key);
	}
}
