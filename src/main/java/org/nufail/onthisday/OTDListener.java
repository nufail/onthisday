package org.nufail.onthisday;

import java.io.IOException;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;

public class OTDListener implements ServletContextListener {

	private static Logger logger = Logger.getLogger(OTDListener.class.getName());

	@Override
	public void contextInitialized(ServletContextEvent event) {
		logger.info("Initializing OTDListener");

		try {

			Properties properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(OTDConstants.OTD_PROPERTY_FILE));
			OTDConstants.setPropertyMap(properties);
			
			String cron = OTDConstants.getProperty(OTDConstants.CRON_PROPERTY);
			
			String key = "org.quartz.impl.StdSchedulerFactory.KEY";
			SchedulerFactory factory = (SchedulerFactory) event.getServletContext().getAttribute(key);
			Scheduler scheduler = factory.getScheduler("OnThisDayScheduler");
	
			// define the job and tie it to our HelloJob class
			JobDetail job = org.quartz.JobBuilder.newJob(OnThisDayJob.class)
					.withIdentity("OTDJob", "group1")
					.usingJobData("lastValue", 0)
					.build();

			// Trigger the job to run now, and then every 40 seconds
			Trigger trigger = org.quartz.TriggerBuilder.newTrigger()
					.withIdentity("OTDTrigger", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule(cron).inTimeZone(TimeZone.getTimeZone("GMT+0530")))
//					.startNow()
//					.withSchedule(org.quartz.SimpleScheduleBuilder.simpleSchedule()
//							.withIntervalInSeconds(3)
//							.withRepeatCount(4))
					.build();
	
			logger.info("Scheduling OTD job. [Scheduler: " + scheduler.getSchedulerName() + "]");
			// Tell quartz to schedule the job using our trigger
			scheduler.scheduleJob(job, trigger);

		} catch (SchedulerException e) {
			logger.log(Level.WARNING,"Exception during scheduling jobs", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while reading OTD properties file", e);
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.info("Destroying context");
//		try {
//			if (scheduler != null && scheduler.isStarted()) {
//				scheduler.shutdown();
//			}
//		} catch (SchedulerException e) {
//			logger.log(Level.WARNING, "Exception during scheduler shutdown", e);
//		}
//		scheduler.shutdownNow();
//		try {
//			scheduler.awaitTermination(20, TimeUnit.SECONDS);
//		} catch (InterruptedException e) {
//			logger.log(Level.WARNING,
//					"scheduler.awaitTermination() interrupted", e);
//		}
	}

}
