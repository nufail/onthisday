package org.nufail.onthisday;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.StatusCodes;
import hms.kite.samples.api.sms.SmsRequestSender;
import hms.kite.samples.api.sms.messages.MtSmsReq;
import hms.kite.samples.api.sms.messages.MtSmsResp;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class OnThisDayJob implements Job {

	private static Logger logger = Logger.getLogger(OnThisDayJob.class.getName());
	
	private static final String lastDayKey = "lastDay";
	private static final String usedYearsKey = "usedYears";
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("Running OnThisDay Job");
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
				
		DateFormat dateFormat = new SimpleDateFormat("MM_dd");
		Date now = new Date();
		String day = dateFormat.format(now);
		
		String lastDay = dataMap.getString(lastDayKey);
		ArrayList<String> usedYears = (ArrayList<String>) dataMap.get(usedYearsKey);

		if (!day.equals(lastDay)) {
			dataMap.put(lastDayKey, day);
			usedYears = new ArrayList<String>();
			dataMap.put(usedYearsKey, usedYears);
		} else if (usedYears == null) {
			usedYears = new ArrayList<String>();
			dataMap.put(usedYearsKey, usedYears);
		}
		
		String message = getMessage(day, usedYears);
		logger.info("MESSAGE===" + message);
		sendMessage(message);
	}
	
	private String getMessage (String day, ArrayList<String> usedYears) {
		Utils utils =  new Utils();
		JSONObject json = utils.readJsonFromFile(day+".json");
		// logger.info("....."+json.toString(1));
		String[] keys = JSONObject.getNames(json);		
		Random random = new Random();
		String randKey;
		int yearBoundary = Integer.parseInt(OTDConstants.getProperty(OTDConstants.YEAR_BOUND_PROPERTY));
		do {
			randKey = keys[random.nextInt(keys.length)];
		} while (usedYears.contains(randKey) || !utils.isLaterYear(randKey, yearBoundary));
		String message = "In " + randKey + ", " + json.optString(randKey);
		
		usedYears.add(randKey);
		
		return message;
	}
	
	private void sendMessage (String message) {
		try {
			SmsRequestSender smsMtSender = new SmsRequestSender(new URL(OTDConstants.getProperty(OTDConstants.SMS_ENDPOINT_PROPERTY)));

			// mtSmsReq = createSubmitMultipleSms(moSmsReq);
			MtSmsReq mtSmsReq = new MtSmsReq();

			mtSmsReq.setApplicationId(OTDConstants.getProperty(OTDConstants.APP_ID_PROPERTY));
			mtSmsReq.setPassword(OTDConstants.getProperty(OTDConstants.APP_PASS_PROPERTY));

			List<String> addressList = new ArrayList<String>();
			addressList.add("tel:all");
//			addressList.add("tel:94777123456");

			mtSmsReq.setDestinationAddresses(addressList);
			mtSmsReq.setMessage(message);

			// mtSmsReq.setSourceAddress("OnThisDay");// default sender address or aliases
			// mtSmsReq.setDeliveryStatusRequest("1");
			// mtSmsReq.setEncoding("0");
			// mtSmsReq.setChargingAmount("5");

			logger.info("REQUEST===" + mtSmsReq.toString());
			MtSmsResp mtSmsResp = smsMtSender.sendSmsRequest(mtSmsReq);

			String statusCode = mtSmsResp.getStatusCode();
			String statusDetails = mtSmsResp.getStatusDetail();
			logger.info("RESPONSE===" + mtSmsResp.toString());

			// Control.saveActionResponse(message, statusCode, statusDetails);

			if (StatusCodes.SuccessK.equals(statusCode)) {
				logger.info("MT SMS message successfully sent");
			} else {
				logger.severe("MT SMS message sending failed with status code ["
						+ statusCode + "] " + statusDetails);
			}

		} catch (MalformedURLException e) {
			logger.log(Level.WARNING, "SMS Endpoint URL incorrect", e);
		} catch (SdpException e) {
			logger.log(Level.SEVERE,
					"Unexpected error occurred while sending SMS Request", e);
		}
	}

}
