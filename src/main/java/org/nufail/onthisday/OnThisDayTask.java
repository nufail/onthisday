package org.nufail.onthisday;

import java.util.logging.Logger;

public class OnThisDayTask implements Runnable {
	
	private static Logger logger = Logger.getLogger(OnThisDayTask.class.getName()); 
	
	private int previous;

	@Override
	public void run() {
		logger.info("===== Running OnThisDayTask =====" + ++previous);
		
	}

}
