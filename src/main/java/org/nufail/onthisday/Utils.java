package org.nufail.onthisday;

import java.io.InputStream;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Utils {
	private static final String eventsdataLocation = "org/nufail/onthisday/eventsdata/";

	public JSONObject readJsonFromFile(String filename) {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(eventsdataLocation + filename);
		JSONObject eventsJson = new JSONObject(new JSONTokener(is));
		return eventsJson;
	}
	
	public boolean isLaterYear(String yearToCheck, int boundaryYear) {
		try {
			int year = Integer.parseInt(yearToCheck);
			return year >= boundaryYear;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
